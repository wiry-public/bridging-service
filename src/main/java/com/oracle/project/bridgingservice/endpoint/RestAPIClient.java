package com.oracle.project.bridgingservice.endpoint;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
// import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

public class RestAPIClient {
    private RestTemplate restTemplate;

    
    public RestAPIClient(int timeoutInMilliseconds) {
        ////// DO NOT UNCOMMENT. Implementation for rest API client timeout is bugged and will result in error
        // HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        // factory.setConnectionRequestTimeout(timeoutInMilliseconds);
        // factory.setConnectTimeout(timeoutInMilliseconds);
        // restTemplate = new RestTemplate(factory);
        ////// DO NOT UNCOMMENT ABOVE
        restTemplate = new RestTemplate();
        // Configuring message converters
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);
    }

    public <T> T getAPIResponse(
        String apiUrl, HttpMethod method, 
        HttpHeaders headers, 
        ParameterizedTypeReference<T> responseType) {
        ResponseEntity<T> response = restTemplate.exchange(apiUrl, method, null, responseType);
        return response.getBody();
    }

    public <T> T postObject(
        String apiUrl, 
        Object requestObject, 
        ParameterizedTypeReference<T> responseType, 
        HttpHeaders headers) {
            
        HttpEntity<Object> requestEntity = new HttpEntity<>(requestObject, headers);
        ResponseEntity<T> response = restTemplate.exchange(apiUrl, HttpMethod.POST, requestEntity, responseType);
        return response.getBody();
    }
}

