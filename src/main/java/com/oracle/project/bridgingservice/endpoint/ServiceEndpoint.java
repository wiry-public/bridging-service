package com.oracle.project.bridgingservice.endpoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

//import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.server.endpoint.annotation.SoapHeader;

import com.oracle.project.bridgingservice.data.BridgingServiceRequestDTO;
import com.oracle.project.bridgingservice.data.BridgingServiceResponseData;
import com.oracle.project.bridgingservice.repo.bridgingRepo;
import com.oracle.project.bridgingservice.soap.ws.soap.AuthenticationHeader;
import com.oracle.project.bridgingservice.soap.ws.soap.Bridgingservicerq;
import com.oracle.project.bridgingservice.soap.ws.soap.Bridgingservicers;
import com.oracle.project.bridgingservice.util.ObjectMapperUtil;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;

@Endpoint
@Service
@Slf4j
public class ServiceEndpoint {
	
	@Autowired
	private bridgingRepo bridgingRepoRequest;

	@Value("${rest.url.bridgingcache}")
	private String endpointBridgingCacheURL;

	@Value("${rest.timeout}")
	private int restTimeoutInMilliseconds;
	

	@PayloadRoot(
		namespace = "http://www.telkomsel.co.id/ufo/services/bridging-service/request/v1.0",
		localPart = "bridgingservicerq")
	@ResponsePayload //annotation makes Spring WS map the returned value to the response payload.
	public Bridgingservicers Service(@RequestPayload JAXBElement<Bridgingservicerq> req, @SoapHeader("{http://www.oracle.com}AuthenticationHeader") SoapHeaderElement auth) {
			
		//Make new object for return
		Bridgingservicers response = new Bridgingservicers();
		
		//Unpack request object from jaxbelement
		Bridgingservicerq request = req.getValue();
		
		//Get the header value
		AuthenticationHeader header = this.getAuthentication(auth);

		String authUsername = "osb";
		String authPassword = "welcome1";
		
		if(authUsername.equalsIgnoreCase(header.getUsername()) && 
		authPassword.equalsIgnoreCase(header.getPassword())) {
			
			//Set the variable value for object responfact
			//List<BridgingServiceResponseData> bridgingServiceResponse = getAllBridgingServiceResponse(request);	// from DB
			List<BridgingServiceResponseData> bridgingServiceResponse = getAllBridgingServiceResponseFromJSONApi(request);  // from JSON Api

			if("w".equalsIgnoreCase(bridgingServiceResponse.get(0).getType()))
			{
				response.setStatus(true);
				response.setErrorCode("0000");
				response.setErrorMessage("Success");
				response.setTrxId(request.getTrxId());
			}
			else if ("b".equalsIgnoreCase(bridgingServiceResponse.get(0).getType())) 
			{
				response.setStatus(false);
				response.setErrorCode("0000");
				response.setErrorMessage("Success");
				response.setTrxId(request.getTrxId());
			}
			else
			{
				response.setStatus(false);
				response.setErrorCode("0000");
				response.setErrorMessage("Success");
				response.setTrxId(request.getTrxId());
			}
			
		}else {
			//Set the variable value for object responfact
			response.setErrorCode("400");
			response.setErrorMessage("Wrong Username & Password");
			response.setTrxId(request.getTrxId());
		}
		return response;
//		return createJaxbElement(response, Bridgingservicers.class);
	}
	
	// set method for response list value from DB
	public List<BridgingServiceResponseData> getAllBridgingServiceResponse(Bridgingservicerq request) {
		List<Map<String, Object>> dataBridging = bridgingRepoRequest.getAllBridging(request);
		List<BridgingServiceResponseData> response = new ArrayList<>();
		
		
		if(dataBridging.size() > 0) {
			
			for(Map<String, Object> data : dataBridging) {

				BridgingServiceResponseData rs = new BridgingServiceResponseData();
				if (data.get("PREFIX")!= null) {
					rs.setPrefix(data.get("PREFIX").toString());	
				}
				if (data.get("SUBSID")!= null) {
					rs.setSubsId(data.get("SUBSID").toString());	
				}
				if (data.get("ORDER_TYPE")!= null) {
					rs.setOrderType(data.get("ORDER_TYPE").toString());	
				}
				if (data.get("SYSTEM")!= null) {
					rs.setSystem(data.get("SYSTEM").toString());	
				}

				if (data.get("TYPE")!= null) {
					rs.setType(data.get("TYPE").toString());	
				}
				if (data.get("V_ID")!= null) {
					rs.setVid(data.get("V_ID").toString());	
				}
				if (data.get("ATTRIBUTES_NAME")!= null) {
					rs.setAttributesNames(data.get("ATTRIBUTES_NAME").toString());	
				}
				if (data.get("ATTRIBUTES_VALUE")!= null) {
					rs.setAttributesValue(data.get("ATTRIBUTES_VALUE").toString());	
				}
	            
	            response.add(rs);

			}

		}
		
		return response;
	}
	
	// same as above, but getting List value from endpoint instead of DB
	public List<BridgingServiceResponseData> getAllBridgingServiceResponseFromJSONApi(Bridgingservicerq request){
		RestAPIClient apiClient = new RestAPIClient(restTimeoutInMilliseconds);

		BridgingServiceRequestDTO newBridgingServiceRequestDTO = new BridgingServiceRequestDTO();
		newBridgingServiceRequestDTO = ObjectMapperUtil.map(request, BridgingServiceRequestDTO.class);
		log.info(String.format("Object to be sent to bridging service has been initialized. newBridgingServiceRequestDTO: %s. Object will be sent to %s", 
			newBridgingServiceRequestDTO, endpointBridgingCacheURL));
		
        // Set headers if required
        HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		// set the data type for the data we will receive later
        ParameterizedTypeReference<List<BridgingServiceResponseData>> responseType = new ParameterizedTypeReference<>() {};

		// fire the API
        List<BridgingServiceResponseData> bridgingServiceResponseDataList = apiClient
			.postObject(endpointBridgingCacheURL, newBridgingServiceRequestDTO, responseType, headers);
		log.info(String.format("Posted the object. received data type: bridgingServiceResponseDataList : %s"
			, bridgingServiceResponseDataList.toString()));
		
		// if list is not empty		
		if(!bridgingServiceResponseDataList.isEmpty()){
			//take only one
			BridgingServiceResponseData oneBridgingServiceResponseData = bridgingServiceResponseDataList.get(0);


			// return the data in list 
			List<BridgingServiceResponseData> returnData = new ArrayList<>();
			returnData.add(oneBridgingServiceResponseData);
			return returnData;
		}		

		return Collections.emptyList(); //return null
	}

	private AuthenticationHeader getAuthentication(SoapHeaderElement header){
		AuthenticationHeader authentication = null;
        try {

            JAXBContext context = JAXBContext.newInstance(AuthenticationHeader.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            authentication = (AuthenticationHeader) unmarshaller.unmarshal(header.getSource());

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return authentication;
    }
	
	//private <T> JAXBElement<T> createJaxbElement(T object, Class<T> clazz) {
	//    return new JAXBElement<>(new QName(clazz.getSimpleName()), clazz, object);
	//}
	
}
