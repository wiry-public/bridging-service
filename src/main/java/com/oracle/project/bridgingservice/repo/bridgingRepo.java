package com.oracle.project.bridgingservice.repo;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.oracle.project.bridgingservice.soap.ws.soap.Bridgingservicerq;

import oracle.jdbc.internal.OracleTypes;

@Repository
public class bridgingRepo {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public List<Map<String, Object>> getAllBridging(Bridgingservicerq request) {
//		Declare method for execute to db
		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withProcedureName("BRIDGING_GET").declareParameters(
				new SqlParameter("IN_MSISDN", Types.VARCHAR), //INPUT
				new SqlParameter("IN_SUBSID", Types.VARCHAR),  //INPUT
				new SqlParameter("IN_ORDER_TYPE", Types.VARCHAR),  //INPUT
				new SqlParameter("IN_SUB_TYPE", Types.VARCHAR),  //INPUT
				new SqlParameter("IN_SYSTEM", Types.VARCHAR),  //INPUT
				new SqlOutParameter("OUT_DATA", OracleTypes.CURSOR)); //OUTPUT
		
		//Insert the data input
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("IN_MSISDN", request.getMsisdn());
		parameter.addValue("IN_SUBSID", request.getSubsid());
		parameter.addValue("IN_ORDER_TYPE", request.getOrderType());
		parameter.addValue("IN_SUB_TYPE", request.getSubType());
		parameter.addValue("IN_SYSTEM", request.getSystem());
		
		//Execute to DB
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> execute = (List<Map<String, Object>>) call.execute(parameter).get("OUT_DATA");
		
//		System.out.println("repo"+execute);
		return execute;
	}
}
