package com.oracle.project.bridgingservice.data;

import lombok.Data;

@Data
public class BridgingServiceRequestDTO {
    // example request payload
    // "msisdn":"6281234567890",
    // "subsid":"",
    // "orderType":"CH",
    // "subType":"PO",
    // "system":"A"
    private String msisdn;
    private String subsid;
    private String orderType;
    private String subType;
    private String system;
}
