package com.oracle.project.bridgingservice.data;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BridgingServiceResponseData {
    // example payload from endpoint bridging cache
    // {
    //     "prefix": "6281234567890",
    //     "subsId": null,
    //     "orderType": "CH",
    //     "system": "A",
    //     "type": "B",
    //     "attributesName": null,
    //     "attributesValue": null,
    //     "pid": 2,
    //     "vid": null
    // }
	private String prefix;
    private String subsId;
    private String orderType;
    private String system;
    private String type;
    private String attributesNames;
    private String attributesValue;
    private Long pid;    
    private String vid;
}
